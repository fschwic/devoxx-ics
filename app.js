/* 
 * DevoxxICS - Converts representations of schedule/events from Devoxx API
 * into ICS.
 * 
 * Written in 2013 by Frank Schwichtenberg fschwic@googlemail.com
 * 
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 * 
 * You should have received a copy of the CC0 Public Domain Dedication along with
 * this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */
var http = require('http');
var https = require('https');
var url = require('url');
var restify = require('restify');

var port = process.env.PORT || 8081;

var devoxxEventNumber = "DevoxxBe2014";

function icsEscape(s){
    return s.replace(/([;])/g, '\\$1').replace(/\r\n|\r|\n/g, '\\n');
}

function dateTimeDevoxx2Ics(dateTimeMillis){
    // new Date(millis).toJSON() -> 2011-05-26T07:56:00.123Z
    var date = new Date(dateTimeMillis);
    var dateTime = date.toJSON();
    return dateTime.replace(/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}).*/g, '$1$2$3T$4$5$6Z');
}

var openRequests = 0;
function iAmReady(res){
  console.log("someone is ready");
  openRequests--;
  if( openRequests == 0 ) {
    res.write("END:VCALENDAR\r\n"); 
    res.end();
  }
}

function respondComplete (req, res, next){
  // console.log("complete");
  openRequests = 5;
  devoxxEventNumber = req.params.event;
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write("BEGIN:VCALENDAR\r\n"); 
  serve(req, res, "monday", iAmReady);
  serve(req, res, "tuesday", iAmReady);
  serve(req, res, "wednesday", iAmReady);
  serve(req, res, "thursday", iAmReady);
  serve(req, res, "friday", iAmReady);
}

function respondUniversity (req, res, next){
  // console.log("university");
  openRequests = 2;
  devoxxEventNumber = req.params.event;
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write("BEGIN:VCALENDAR\r\n"); 
  serve(req, res, "monday", iAmReady);
  serve(req, res, "tuesday", iAmReady);
}

function respondConference (req, res, next){
  // console.log("conference");
  openRequests = 3;
  devoxxEventNumber = req.params.event;
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write("BEGIN:VCALENDAR\r\n"); 
  serve(req, res, "wednesday", iAmReady);
  serve(req, res, "thursday", iAmReady);
  serve(req, res, "friday", iAmReady);
}

function respondDay (req, res, next){
  // console.log("day");
  openRequests = 1;
  devoxxEventNumber = req.params.event;
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write("BEGIN:VCALENDAR\r\n"); 
  serve(req, res, req.params.dayOfWeek, iAmReady);
}

function serve (req, res, suf, onReady){

    http.request({host: 'cfp.devoxx.be', path: '/api/conferences/' + devoxxEventNumber + '/schedules/' + suf, 
      headers: {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'}, method: 'GET'}, function (scheduleResponse){
      var schedulesJson = "";
      console.log('Schedules Request: HTTP ' + scheduleResponse.statusCode);
      console.log(req.path);

      function createEntry(slot){
        if(slot.talk){
          var entry = ""

          entry += "BEGIN:VEVENT\r\n";
          entry += "UID:" + icsEscape(slot.talk.id) + "\r\n";
          entry += "SUMMARY:" + icsEscape(slot.talk.title + " by " + slot.talk.speakers[0].name) + "\r\n";
          entry += "DESCRIPTION:" + icsEscape("Track: " + slot.talk.track);
          if (slot.talk.lang != "en"){
            entry += icsEscape(", " + slot.talk.lang);
          }
          entry += icsEscape(". \n\n" + slot.talk.summary) + "\r\n";
          entry += "CATEGORIES:" + icsEscape(slot.talk.track) + "\r\n";
          entry += "LOCATION:" + icsEscape(slot.roomName) + "\r\n";
          entry += "DTSTART:" + dateTimeDevoxx2Ics(slot.fromTimeMillis) + "\r\n";
          entry += "DTEND:" + dateTimeDevoxx2Ics(slot.toTimeMillis) + "\r\n";
          entry += "X-SPEAKER;FMTTYPE=application/json:" + icsEscape(slot.talk.speakers[0].link.href) + "\r\n";
          entry += "END:VEVENT\r\n";

          return entry;
        }
        else {
          return "";
        }

      }

      scheduleResponse.on('data', function (data){
        // collect the entire response
        schedulesJson += data;
      }).on('end', function (){
        // response complete
        var schedules = JSON.parse(schedulesJson);
              
        for (var i = 0; i < schedules.slots.length; i++) {
          var entry = createEntry(schedules.slots[i]);
          res.write(entry);
        }
        onReady(res);
      });

    }).on('error', function (e){
      console.log("Schedule Request Error: " + e.message);
      //console.log(JSON.parse(e));
    }).end();
    
}

var server= restify.createServer();
server.get('/favicon.ico', function(req, res, next){
  res.send(404);
  next();
});
server.get('/:event', respondComplete);
server.get('/:event/uni', respondUniversity);
server.get('/:event/conf', respondConference);
server.get('/:event/:dayOfWeek', respondDay);

server.listen(port, '0.0.0.0', function() {
    console.log('%s listening at %s', server.name, server.url);
});
console.log('Server running.');
