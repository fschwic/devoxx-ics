### tl;dr ###
Because of some questions: If you want to include the Devoxx schedule into your calendar application, subscribe (try to _subscribe_! if you just click and import you have a static copy which does not update.) the following URL.

 http://devoxx-ics.herokuapp.com/DevoxxBe2014

If you want a webpage that shows the talks of the next hour (useful not before 10.11.2014) open the following URL in a browser (respectively bookmark it).

 <http://core.epoko.net/view/list?cal_markup=markdown&cal_standalone=yes&cal_css_prefix=cal&cal_title=Devoxx%20now&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014&cal_hour=-0&cal_minute=-0&cal_length=1H>

More details see below.

## DevoxxICS ##
**Update for Devoxx 2014 at 11.10.2014**

DevoxxICS is a project to convert the schedule of Devoxx conferences into ICS in order
to get it into calender apps and to easily include it into webpages. 
By subscribing to the ICS source every change to the schedule will appear
automatically in the local view. I use it with iPad calendar, thunderbird ligthning, 
[ICS Sync](http://www.icssync.com) on android, and others. 

You can see the ICS events in a webpage by caling the listview by EPOKO.net:
<http://core.epoko.net/view/list?cal_markup=markdown&cal_standalone=yes&cal_css_prefix=cal&cal_title=Devoxx%202014&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014>
More options using EPOKO see below.

The schedule of Defoxx conferences is provided by the orgnaizers
of [Devoxx](http://www.devoxx.be) via a REST interface that is described at <http://cfp.devoxx.be/api>. The schedule of Devoxx 2014 can be seen at the Devoxx website: <http://cfp.devoxx.be/2014>

For Devoxx 2014 the REST interface was changed. Before it could  be retrieved via 
<http://cfp.devoxx.com/rest/v1/events/10/schedule>. The old REST interface is described at
<http://www.devoxx.com/display/Devoxx2K10/Schedule+REST+interface>. 

Please report issues at [Bitbucket issue tracker](https://bitbucket.org/fschwic/devoxx-ics/issues).


### Usage ###
Occasionally devoxx-ics is deployed on <http://devoxx-ics.herokuapp.com> where you can 
try it.

[/DevoxxBe2014](http://devoxx-ics.herokuapp.com/DevoxxBe2014) responds with the entire schedule of Devoxx 2014.

[/DevoxxBe2014/uni](http://devoxx-ics.herokuapp.com/DevoxxBe2014/uni) responds with the schedule of the first two days, the Devoxx University.

[/DevoxxBe2014/conf](http://devoxx-ics.herokuapp.com/DevoxxBe2014/conf) responds with the schedule of the third, forth, and fifth day, the Devoxx Conference.

[/DevoxxBe2014/monday](http://devoxx-ics.herokuapp.com/DevoxxBe2014/monday) responds with the schedule of the first day.

[/DevoxxBe2014/tuesday](http://devoxx-ics.herokuapp.com/DevoxxBe2014/tuesday) responds with the schedule of the second day.

[/DevoxxBe2014/wednesday](http://devoxx-ics.herokuapp.com/DevoxxBe2014/wednesday) responds with the schedule of the third day.

[/DevoxxBe2014/thursday](http://devoxx-ics.herokuapp.com/DevoxxBe2014/thursday) responds with the schedule of the fourth day.

[/DevoxxBe2014/friday](http://devoxx-ics.herokuapp.com/DevoxxBe2014/friday) responds with the schedule of the fifth day.


#### Usage for Devoxx 2013 ####

[/](http://devoxx-ics.herokuapp.com/) responds with the entire schedule of Devoxx 2013.

[/1](http://devoxx-ics.herokuapp.com/) responds with the schedule of the first day.

[/2](http://devoxx-ics.herokuapp.com/) responds with the schedule of the second day.

[/3](http://devoxx-ics.herokuapp.com/) responds with the schedule of the third day.

[/4](http://devoxx-ics.herokuapp.com/) responds with the schedule of the fourth day.

[/5](http://devoxx-ics.herokuapp.com/) responds with the schedule of the fifth day.


#### EPOKO.webview ####
You may combine the results with webviews from [EPOKO.net](http://www.EPOKO.net) which 
generate HTML snippets that are meant to be included in webpages:

The entire schedule in list view:
<http://core.epoko.net/view/list?cal_markup=markdown&cal_standalone=yes&cal_css_prefix=cal&cal_title=Devoxx%202014&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014&cal_day=1&cal_month=11&cal_year=2014>

The talks of the next hour (useful while being at Devoxx :-) ):
<http://core.epoko.net/view/list?cal_markup=markdown&cal_standalone=yes&cal_css_prefix=cal&cal_title=Devoxx%20now&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014&cal_hour=-0&cal_minute=-0&cal_length=1H>

The entire schedule in week view:
<http://core.epoko.net/view/week?cal_title=Devoxx2014%20(complete)&cal_week=46&cal_year=2014&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014>.

The second day in list view:
<http://core.epoko.net/view/list?cal_title=Devoxx2014%20Tuesday&cal_day=1&cal_month=11&cal_year=2014&cal_standalone=yes&cal_webfile=http://devoxx-ics.herokuapp.com/DevoxxBe2014/tuesday>.

### Run ###
It's a node.js app. Install node.js and 

    $ npm install
    $ node app.js


### Devoxx REST API ###
See <http://cfp.devoxx.be/api>